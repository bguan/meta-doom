SUMMARY = "EAWPATS GUS patches for Doom, Heretic, and similar"
DESCRIPTION = "This is a collection of Gravis UltraSound patches for use with \
a GUS, or with softsynth midi players such as Cubic Player and TiMidity. This \
is a solution for really good MIDI sound in Doom, Heretic, and similar."
HOMEPAGE = "https://www.doomworld.com/idgames/sounds/eawpats"

SECTION = "games"

LICENSE = "CLOSED"
LIC_FILES_CHKSUM = "file://${WORKDIR}/eawpats.txt"

SRC_URI = " \
    ftp://ftp.fu-berlin.de/pc/games/idgames/sounds/eawpats.zip;name=zip;subdir=eawpats \
    ftp://ftp.fu-berlin.de/pc/games/idgames/sounds/eawpats.txt;name=txt \
"
SRC_URI[zip.md5sum] = "323c540c5a7923f7cf5a220fc1b7f4a4"
SRC_URI[zip.sha256sum] = "19087fa4a40e25ec39a09cffcc9f775fc22d88bc971a7a9831e075cdae2ee1e3"
SRC_URI[txt.md5sum] = "cf60d250b09cf7ce1a9906fbb5734e50"
SRC_URI[txt.sha256sum] = "d7ddf46f9e6930b85a802021eb041bbc975a5cc5064d8f940408141550f0e339"

inherit allarch

do_install() {
    install -d ${D}/${datadir}/games/eawpats
    cp -R --no-preserve=ownership ${WORKDIR}/eawpats/ ${D}/${datadir}/games/
}

FILES_${PN} = "${datadir}/games/eawpats"
