SUMMARY = "Freedoom supplies free content for games based on the Doom engine."
DESCRIPTION = "The Freedoom project aims to create a complete free content \
game based on the Doom engine."
HOMEPAGE = "https://freedoom.github.io/"
BUGTRACKER= "https://github.com/freedoom/freedoom/issues"

SECTION = "games"

LICENSE = "BSD-3-Clause"
LIC_FILES_CHKSUM = "file://COPYING.txt;md5=038918b78710d44563f923bd8119f814"

SRC_URI = "https://github.com/freedoom/freedoom/releases/download/v${PV}/freedoom-${PV}.zip"
SRC_URI[md5sum] = "f8c86928394b1d425ef60637b5e8ac31"
SRC_URI[sha256sum] = "f42c6810fc89b0282de1466c2c9c7c9818031a8d556256a6db1b69f6a77b5806"

inherit allarch

do_install() {
    install -d ${D}/${datadir}/games/doom
    install -m 0644 ${WORKDIR}/freedoom-${PV}/freedoom1.wad ${D}/${datadir}/games/doom/
    install -m 0644 ${WORKDIR}/freedoom-${PV}/freedoom2.wad ${D}/${datadir}/games/doom/

    install -d ${D}/${docdir}/freedoom
    install -m 0644 ${WORKDIR}/freedoom-${PV}/freedoom-manual.pdf ${D}/${docdir}/freedoom/
}

FILES_${PN} = "${datadir}/games/doom"
FILES_${PN}-doc = "${docdir}/freedoom"
