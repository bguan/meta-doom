# meta-doom

This meta-layer supplies recipes for compiling Doom from id Software with Yocto Project.

The meta-layer is has been tested on Poky's core-image-minimal using qemux86 with Yocto Project 3.1 "Dunfell" and 3.2 "Gatesgarth".

## Setup

Create a directory `yocto` (or any other name of your choice) and clone Yocto Project Poky and meta-doom into it.

```
mkdir yocto
cd yocto
git clone https://git.yoctoproject.org/git/poky
git clone https://gitlab.com/toertel/meta-doom.git
```

Copy supplied `bblayers.conf` or create your own.

```
cp meta-doom/conf/bblayers.conf.sample poky/build/conf/bblayers.conf
```

Copy Poky's `local.conf.sample` or create your own.

```
cp poky/meta-poky/conf/local.conf.sample poky/build/conf/local.conf
```

### DirectFB (framebuffer)
Append the following lines to your `local.conf`.

```
MACHINE ?= "qemux86-64"

DISTRO_FEATURES_append = " directfb"
DISTRO_FEATURES_remove = "x11"

IMAGE_INSTALL_append = " chocolate-doom"

# WORKAROUND: DirectFB libraries are not installed although libsdl2 has it in its DEPENDS.
IMAGE_INSTALL_append = " directfb"
```

### X11
Append the following lines to your `local.conf`.

```
MACHINE ?= "qemux86-64"

IMAGE_FEATURES += "x11-base"

IMAGE_INSTALL_append = " chocolate-doom"
```

## Build

Source build environment and build _core-image-base_.

```
cd poky
source oe-init-build-env
bitbake core-image-minimal
```

**Note:** Because of a so far unknown reason the DirectFB libraries are not installed even though _libsdl2_ has _directfb_ in its DEPENDS as well as in its PACKAGECONFIG. Manually adding _directfb_ installs the libraries.

## Run

### DirectFB (framebuffer)

Start QEMU with the image built.

```
runqemu core-image-minimal kvm slirp serial
```

Login as user _root_ and and start `chocolate-doom`.

```
chocolate-doom
```

### X11

Start QEMU with the image built.

```
runqemu core-image-minimal kvm slirp serial gl sdl
```

Login as user _root_ and and start `chocolate-doom`.

```
DISPLAY=:0 chocolate-doom
```

**Note:** In starting of QEMU fails because _package dri was not found_ you need to instal the MESA development package of your distribution. For Ubuntu `sudo apt install mesa-common-dev` does the trick.
