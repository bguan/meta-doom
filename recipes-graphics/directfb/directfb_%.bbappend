FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI += "file://directfbrc"

do_install_append() {
    install -d ${D}/${sysconfdir}
    install -m 644 ${WORKDIR}/directfbrc ${D}/${sysconfdir}
}
